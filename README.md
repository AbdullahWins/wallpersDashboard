# Wallpers Dashboard (Admin Panel)

Wallpers is an app that provides unique wallpapers and ringtones to customize your mobile experience!

================================================================================================

## Live Website Link:

You can visit the live demo of this app here: [Wallpers Admin Panel](https://wallpers.netlify.app/).

================================================================================================

### Phase:

In Development!
